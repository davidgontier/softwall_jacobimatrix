# SoftWall_JacobiMatrix

This is companion code for the article "Edge states for Jacobi operators with soft walls", by Camilo Gómez Araya, David Gontier and Hanne Van Den Bosch

Copyright: The code was written by David Gontier (December 2023). It may be reproduced, in its entirety, for non-commercial purpose.
